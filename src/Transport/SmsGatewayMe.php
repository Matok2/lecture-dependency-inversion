<?php

namespace Matok\Transport;

use Matok\Sms\Sms;
use Matok\Util\RemoteFile;

class SmsGatewayMe
{
    private $remoteFile;

    private $secret;

    public function __construct(RemoteFile $remoteFile, $endPoint, $secret)
    {
        echo __CLASS__." - construct \n";

        $remoteFile->setTargetUrl($endPoint);
        $this->remoteFile = $remoteFile;
    }


    public function send(Sms $sms, $number)
    {
        $params = array(
            'secret' => $this->secret,
            'sms' => $sms->getContent(),
            'from' => $sms->getFrom(),
            'to' => $number,
        );
        echo __CLASS__." - sending SMS: params [".serialize($params)."] \n";


        $this->remoteFile->getContent($params);
    }
}