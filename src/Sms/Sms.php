<?php
namespace Matok\Sms;

use Matok\Transport\SmsGatewayMe;

class Sms
{
    private $from;

    private $content;

    /** @var SmsGatewayMe */
    private $transport;

    public function __construct($from, $content)
    {
        echo __CLASS__." - construct \n";

        $this->from = $from;
        $this->content = $content;
    }

    public function setTransport(SmsGatewayMe $transport)
    {
        echo __CLASS__." - setting transport to [".get_class($transport)."]\n";
        $this->transport = $transport;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function send($recipientNumber)
    {
        echo __CLASS__." - sending SMS to number [$recipientNumber]\n";
        $this->transport->send($this, $recipientNumber);
    }
}