<?php

namespace Matok\Util;

class RemoteFile
{

    public function __construct()
    {
        echo __CLASS__." - construct \n";
    }

    public function setTargetUrl($url)
    {
        echo __CLASS__." - setting url to [$url] \n";
    }

    public function getContent()
    {
        echo __CLASS__." - getting content \n";
    }
}