# Lecture: Dependency Inversion Principle

## Install
```
composer install
```

## Run
```
php app.php
```

## Tasks

1. examine application to SMS sending
2. SMS (High Level) depends on SmsGatewayMe transport (Low Level) - our goal is rewrite SMS to depends on abstraction (explanation: we can use another gateway to send SMS)
3. new class (_abstraction of SMS transport_) now depends on SMS class - BOTH should depends on abstraction (explanation: application like FB messenger)
