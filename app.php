<?php

include __DIR__.'/vendor/autoload.php';

use Matok\Sms\Sms;
use Matok\Transport\SmsGatewayMe;
use Matok\Util\RemoteFile;

$remoteFile = new RemoteFile();
$transport = new SmsGatewayMe($remoteFile, 'https://smsgateway.me', 'not-a-secret');

$sms = new Sms('Matok', 'I hope you enjoy this lecture.');
$sms->setTransport($transport);

$sms->send('0905123123');